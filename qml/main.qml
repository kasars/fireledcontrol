import QtQuick 2.12
import QtQuick.Controls 2.9
import QtQuick.Layouts 1.15
import QBlueStick 1.0

ApplicationWindow {
    visible: true
    width: 1376
    height: 786
    title: qsTr("Bluetooth Color Selector")
    Rectangle {
        anchors.fill: parent
        color: "#CCCCCC"
    }

    readonly property double pageMargins: height * 0.01

    RowLayout {
        id: rowLayout
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: gradientItem.top
            bottomMargin: pageMargins * 2
        }
        ColumnLayout {
            id: briContainer
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: pageMargins * 24
            Layout.preferredWidth: pageMargins * 30
            Layout.topMargin: pageMargins * 2
            Layout.leftMargin: pageMargins

            Text {
                id: labelItem
                Layout.alignment: Qt.AlignHCenter
                font.pixelSize: pageMargins * 4
                text: qsTr("Brightness")
            }

            ColorBar {
                id: briBar
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: pageMargins *0.35
                Layout.minimumWidth: pageMargins * 12
                barColor: "#FFFFFF"
                Component.onCompleted: setValue(1);
                onValueChanged: { sendTmr.running = true; }
            }
        }

        ColorSelector {
            id: coldColor
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: pageMargins * 2
            label: qsTr("Cold Color")
            startColor: "#FF4400"
            onSelectedColorChanged: { sendTmr.running = true; }
        }

        ColorSelector {
            id: hotColor
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.topMargin: pageMargins * 2
            label: qsTr("Hot Color")
            startColor: "#FFFF20"
            onSelectedColorChanged: { sendTmr.running = true; }
        }
    }

    Rectangle {
        id: gradientItem
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: pageMargins
        }

        border.color: "black"
        border.width: 1
        radius: pageMargins
        height: pageMargins * 10

        gradient: Gradient {
            orientation: Gradient.Horizontal
            GradientStop { position: 0.0; color: coldColor.selectedColor }
            GradientStop { position: 1.0; color: hotColor.selectedColor }
        }

        Rectangle {
            id: brightnessShade
            anchors.fill: parent
            radius: parent.radius
            color: "black"
            opacity: 0.7 - (briBar.value)
        }

        Rectangle {
            id: pressShade
            anchors.fill: parent
            radius: parent.radius
            color: "black"
            opacity: touch1.pressed? 0.4 : 0
        }

        Text {
            anchors.centerIn: parent
            font.pixelSize: pageMargins * 4
            font.bold: true
            text: qsTr("Save colors")
        }
        Text {
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: -2
            anchors.verticalCenterOffset: -2
            font.pixelSize: pageMargins * 4
            font.bold: true
            text: qsTr("Save colors")
            color: "white"
            opacity: 1 - briBar.value
        }

        MultiPointTouchArea {
            anchors.fill: parent
            minimumTouchPoints: 1
            maximumTouchPoints: 1
            touchPoints: [
            TouchPoint {
                id: touch1
                onPressedChanged: {
                    if (!pressed && y > 0 && y <= height && x > 0 && x <= width) {
                        var jsonStr = JSON.stringify({ "saveValues": true});
                        btCom.sendString(jsonStr + "\n");
                    }
                }
            }
            ]
        }

    }

    Timer {
        id: sendTmr
        interval: 100

        onTriggered: {
            if (!btCom.connected) return;
            var c = coldColor.selectedColor;
            var h = hotColor.selectedColor;
            var jsonStr = JSON.stringify({ "cr": Math.round(c.r*255), "cg": Math.round(c.g*255), "cb": Math.round(c.b*255),
                "hr": Math.round(h.r*255), "hg": Math.round(h.g*255), "hb": Math.round(h.b*255),
                "brightness": Math.round(briBar.value*255)});

            btCom.sendString(jsonStr + "\n");
        }
    }

    ScanningView {
    anchors.centerIn: parent
    width: parent.width * 0.5
    height: parent.height * 0.6
    radius: pageMargins*2
    model: btCom.devices
    deviceAddrs: btCom.deviceAddrs
    scanning: btCom.scanning
    connectingIndex: btCom.connectingIndex

    onSelectItem: btCom.connectToService(index)
    onReScan: btCom.scannForDevices()

    visible: !btCom.connected
    }

    BtComModel {
        id: btCom

        onDataReceived: {
            console.log(data);
            var obj = JSON.parse(data);

            if (typeof obj.brightness !== "undefined") {
                coldColor.startColor = Qt.rgba(obj.cr/255.0, obj.cg/255.0, obj.cb/255.0, 1);
                hotColor.startColor = Qt.rgba(obj.hr/255.0, obj.hg/255.0, obj.hb/255.0, 1);
                briBar.setValue(obj.brightness/255.0);
            }
        }
        onConnectedChanged: {
            if (connected) {
                var jsonStr = JSON.stringify({ "getValues": true});
                btCom.sendString(jsonStr + "\n");
            }
        }
    }
}
