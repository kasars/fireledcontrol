import QtQuick 2.15
import QtQuick.Dialogs 1.3

Item {
    id: colorSelector
    readonly property color selectedColor: colorRectangle.color

    property alias label: labelItem.text
    property color startColor: "#FF5500"

    readonly property double barWidth: pageMargins * 12
    readonly property double barSpacing: pageMargins * 4

    readonly property double prefBarsWidth: barWidth * 3 + barSpacing * 2

    implicitWidth: prefBarsWidth

    onStartColorChanged: {
        redBar.setValue(startColor.r);
        greenBar.setValue(startColor.g);
        blueBar.setValue(startColor.b);
    }

    Text {
        id: labelItem
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        font.pixelSize: pageMargins * 4
    }

    Rectangle {
        id: colorRectangle
        anchors {
            top: labelItem.bottom
            horizontalCenter: parent.horizontalCenter
            margins: pageMargins
        }
        width: prefBarsWidth - (pageMargins * 2) // -2*pm == align to visible bars
        height: colorSelector.height * 0.2
        radius: pageMargins * 2

        color: Qt.rgba(redBar.value, greenBar.value, blueBar.value, 1)

        MouseArea {
            id: btnMArea
            anchors.fill: parent
            onClicked: {
                colorDialog.currentColor = colorRectangle.color;
                colorDialog.visible = true;
            }
            Rectangle {
                anchors.fill: parent
                color: "#555555"
                opacity: 0.5
                visible: btnMArea.pressed
                radius: colorRectangle.radius
            }
        }
    }

    Row {
        anchors {
            top: colorRectangle.bottom
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            topMargin: pageMargins * 2
        }
        spacing: pageMargins * 4
        ColorBar {
            id: redBar
            height: parent.height
            width: pageMargins * 12
            barColor: "#FF0000"
            Component.onCompleted: redBar.setValue(startColor.r)
        }
        ColorBar {
            id: greenBar
            height: parent.height
            width: pageMargins * 12
            barColor: "#00FF00"
            Component.onCompleted: greenBar.setValue(startColor.g)
        }
        ColorBar {
            id: blueBar
            height: parent.height
            width: pageMargins * 12
            barColor: "#0000FF"
            Component.onCompleted: blueBar.setValue(startColor.b)
        }

    }

    ColorDialog {
        id: colorDialog
        title: qsTr("Please choose a color for %1").arg(label)

        onAccepted: {
            redBar.setValue(colorDialog.color.r);
            greenBar.setValue(colorDialog.color.g);
            blueBar.setValue(colorDialog.color.b);
        }
    }
}
