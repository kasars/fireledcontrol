import QtQuick 2.3
import QtQuick.Controls 2.9

Rectangle {
    id: scanView
    property alias model: listView.model
    property variant deviceAddrs: []

    property bool scanning: true
    property int connectingIndex: -1

    onConnectingIndexChanged: {
        console.log("connecting::", connectingIndex);
    }

    signal selectItem(int index)
    signal reScan()

    color: "white"
    border.width: 2
    border.color: "#A0A0A0"

    ScrollView {
        anchors.fill: parent
        anchors.margins: parent.radius
        clip: true
        ListView {
            id: listView

            delegate: Rectangle {
                height: scanView.height * 0.24
                width: listView.width
                color: mArea.pressed ? "#C0C0C0" : "white"
                Rectangle {
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    height: 1
                    color: "#A0A0A0"
                    visible: index > 0
                }
                Text {
                    id: textItem
                    anchors.centerIn: parent
                    anchors.verticalCenterOffset: -parent.height/6
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    font.pixelSize: parent.height / 3
                    text: scanView.connectingIndex === model.index ? qsTr("Connecting to: %1").arg(modelData) : modelData
                }
                Text {
                    id: addressItem
                    anchors.centerIn: parent
                    anchors.verticalCenterOffset: parent.height/6

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    font.pixelSize: parent.height / 5
                    text: scanView.deviceAddrs[model.index]
                }
                MouseArea {
                    id: mArea
                    anchors.fill: parent
                    onClicked: {
                        scanView.selectItem(model.index);
                    }
                }
                opacity: scanView.connectingIndex < 0 || scanView.connectingIndex === model.index ? 1.0 : 0.4
            }
        }
    }

    MouseArea {
        id: mouseSchield
        anchors.fill: parent
        visible: scanView.connectingIndex !== -1
        Rectangle {
            anchors.fill: parent
            radius: scanView.radius
            color: "#22000000"
        }
    }

    Text {
        id: statusText
        anchors.centerIn: parent
        font.pixelSize: scanView.height / 10
        text: scanning ? qsTr("Scanning for devices...") : qsTr("No devices found.")
        visible: scanning || listView.count == 0
    }

    BtButton {
        anchors {
            top: statusText.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: parent.height/10
        }
        font.pixelSize: height/4
        text: qsTr("Restart device scan")

        onPressedChanged: {
            if (pressed) {
                scanView.reScan();
            }
        }
        visible: !scanning && listView.count == 0
    }
}
