import QtQuick 2.3

Rectangle {
    id: button
    //implicitHeight: textItem.implicitHeight * 2
    //implicitWidth: textItem.implicitWidth + textItem.implicitHeight
    color: pressed ? "#30A0A0A0" : "#30FFFFFF"
    border.color: "#A0A0A0"
    radius: pageMargins

    signal clicked()

    readonly property bool pressed: touch1.pressed
    property alias text: textItem.text
    property alias font: textItem.font

    property bool activity: false

    Text {
        id: textItem
        anchors.fill: parent

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        wrapMode: Text.Wrap
        elide: Text.ElideRight

        color: button.activity ? "red" : "black"
    }

    MultiPointTouchArea {
        anchors.fill: parent
        minimumTouchPoints: 1
        maximumTouchPoints: 1
        touchPoints: [
            TouchPoint {
                id: touch1
                onPressedChanged: { if (!pressed && y > 0 && y <= height && x > 0 && x <= width) { button.clicked() } }
            }
        ]
    }
}
