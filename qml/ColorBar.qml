import QtQuick 2.3
import QtQuick.Dialogs 1.2

Item {
    id: colorBar
    property color barColor: "#FF0000"
    readonly property double value: 1 - (valRect.y / (height - valRect.height))

    function setValue(newValue) {
        var maxY = colorBar.height - valRect.height;
        valRect.y = maxY - (newValue * maxY);
        valRect.value = newValue;
    }

    onHeightChanged: setValue(valRect.value)

    Rectangle {
        id: bgRect
        anchors{
            fill: parent
            leftMargin: pageMargins
            rightMargin: pageMargins
        }
        border.color: "black"
        border.width: 1
        radius: pageMargins
        gradient: Gradient {
            GradientStop { position: 0.0; color: barColor }
            GradientStop { position: 1.0; color: "#000000" }
        }
    }

    Rectangle {
        id: valRect
        property double value
        width: parent.width
        height: parent.height * 0.05
        border.color: "black"
        border.width: 1
        radius: height/3
        color: "white"
    }

    MultiPointTouchArea {
        anchors.fill: parent
        minimumTouchPoints: 1
        maximumTouchPoints: 1

        touchPoints: [
        TouchPoint {
            id: touch1
            property real lastY: -1
            onPressedChanged: { if (pressed) { lastY = -1; } }
        }
        ]

        onTouchUpdated: {
            if (touch1.lastY == -1) touch1.lastY = touch1.y;
            var newY = valRect.y - (touch1.lastY - touch1.y);
            newY = Math.min(Math.max(0, newY), colorBar.height-valRect.height);
            touch1.lastY = touch1.y;
            valRect.y = newY;
            valRect.value = colorBar.value
        }
    }
}
