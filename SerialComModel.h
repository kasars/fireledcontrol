/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef BtComModel_H
#define BtComModel_H

#include <QObject>
#include <QString>

class BtComModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList devices READ devices NOTIFY devicesChanged)
    Q_PROPERTY(QStringList deviceAddrs READ deviceAddrs NOTIFY devicesChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
    Q_PROPERTY(bool scanning READ scanning NOTIFY scanningChanged)
    Q_PROPERTY(int connectingIndex READ connectingIndex NOTIFY connectedChanged)
    Q_PROPERTY(bool bluetoothAvailable READ bluetoothAvailable NOTIFY bluetoothAvailableChanged)

public:

    explicit BtComModel(QObject *parent = 0);
    ~BtComModel();

    // Bluetooth
    bool connected() const;
    int connectingIndex() const;
    bool bluetoothAvailable() const;
    const QStringList devices() const;
    const QStringList deviceAddrs() const;
    bool scanning() const;
    Q_INVOKABLE void connectToService(int index);

    Q_INVOKABLE void scannForDevices();

    Q_INVOKABLE bool sendString(const QString &str);

Q_SIGNALS:
    void connectedChanged();
    void scanningChanged();
    void devicesChanged();
    void bluetoothAvailableChanged();
    void configChanged();

    void dataReceived(const QString &data);

private:
    class Private;
    friend class Private;
    Private *const d;

};

#endif
