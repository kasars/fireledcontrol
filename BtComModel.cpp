/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "BtComModel.h"

#include <QVector>
#include <QTimer>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothSocket>
#include <QBluetoothLocalDevice>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QAndroidJniEnvironment>
QAndroidJniEnvironment jniEnv;
#endif

class BtComModel::Private {
public:

    Private(BtComModel *q_): q(q_) {}

    void addService(const QBluetoothServiceInfo &serviceInfo) {
        qDebug() << "Discovered service on" << serviceInfo.device().name() << serviceInfo.device().address().toString();
        qDebug() << "\tService name:" << serviceInfo.serviceName();
        qDebug() << "\tDescription:"  << serviceInfo.attribute(QBluetoothServiceInfo::ServiceDescription).toString();
        qDebug() << "\tProvider:"     << serviceInfo.attribute(QBluetoothServiceInfo::ServiceProvider).toString();
        qDebug() << "\tL2CAP protocol service multiplexer:" << serviceInfo.protocolServiceMultiplexer();
        qDebug() << "\tRFCOMM server channel:" << serviceInfo.serverChannel();

        serviceInfos << serviceInfo;
        devices <<  serviceInfo.device().name();
        deviceAddrs << serviceInfo.device().address().toString();
    }

    void readSocket() {
        if (!socket) {
            qDebug() << "No socket";
            return;
        }
        while (socket->canReadLine()) {
            QByteArray btData = socket->readLine();
            while (btData.endsWith('\r') || btData.endsWith('\n')) {
                btData.remove(btData.size()-1, 1);
            }
            emit q->dataReceived(btData);
        }
    }

    #ifdef Q_OS_ANDROID
    QMap<QString,QString> getKnownDevices()
    {
        QString serialportUuid = QBluetoothUuid(QBluetoothUuid::SerialPort).toString();
        serialportUuid.remove("{");
        serialportUuid.remove("}");
        QMap<QString,QString> result;
        /** Reference to Java object android.bluetooth.BluetoothAdapter */
        QAndroidJniObject adapter = QAndroidJniObject::callStaticObjectMethod("android/bluetooth/BluetoothAdapter",
                                                                              "getDefaultAdapter",
                                                                              "()Landroid/bluetooth/BluetoothAdapter;");
        QAndroidJniObject pairedDevicesSet = adapter.callObjectMethod("getBondedDevices", "()Ljava/util/Set;"); // Set<BluetoothDevice>
        jint size=pairedDevicesSet.callMethod<jint>("size");
        qDebug("Found %i paired devices",size);
        if (size>0) {
            QAndroidJniObject iterator=pairedDevicesSet.callObjectMethod("iterator", "()Ljava/util/Iterator;"); // Iterator<BluetoothDevice>
            for (int i=0; i<size; i++) {
                QAndroidJniObject dev = iterator.callObjectMethod("next", "()Ljava/lang/Object;"); // BluetoothDevice
                QString address = dev.callObjectMethod("getAddress", "()Ljava/lang/String;").toString();
                QString name = dev.callObjectMethod("getName", "()Ljava/lang/String;").toString();
                qDebug() << name << address;
                QAndroidJniObject uuids = dev.callObjectMethod("getUuids", "()[Landroid/os/ParcelUuid;");
                int arrayLength = jniEnv->GetArrayLength(uuids.object<jarray>());
                for (int j=0; j<arrayLength; ++j) {
                    QAndroidJniObject arrayElement = jniEnv->GetObjectArrayElement(uuids.object<jobjectArray>(), j);
                    qDebug() << arrayElement.toString();
                    if (arrayElement.toString() == serialportUuid) {
                        result.insert(address, name);
                        break;
                    }
                }
            }
        }
        return result;
    }
    #endif

    BtComModel *q;
    QByteArray axis;
    quint8 buttons = 0;
    QBluetoothLocalDevice            localDevice;
    QBluetoothServiceDiscoveryAgent *discoveryAgent = nullptr;
    QBluetoothDeviceDiscoveryAgent  *devDiscoveryAgent = nullptr;
    QBluetoothSocket                *socket = nullptr;
    bool                             connected = false;
    QStringList                      devices;
    QStringList                      deviceAddrs;
    QVector<QBluetoothServiceInfo>   serviceInfos;
    int                              connectingIndex=-1;
};

BtComModel::BtComModel(QObject *parent) : QObject(parent), d(new Private(this))
{
    d->discoveryAgent = new QBluetoothServiceDiscoveryAgent(this);

    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::serviceDiscovered, this, [this](const QBluetoothServiceInfo &service) {
        d->addService(service);
    });


    d->devDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
    connect(d->devDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this, [](const QBluetoothDeviceInfo &info) {
                qDebug() << info.name() << info.address().toString();
            });

    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::finished, this, [this]() {
        qDebug() << "Scan done";
        emit devicesChanged();
        emit scanningChanged();
    });
    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::canceled, this, [this]() {
        qDebug() << "Scan canceled";
        emit devicesChanged();
        emit scanningChanged();
    });
    connect(d->discoveryAgent, QOverload<QBluetoothServiceDiscoveryAgent::Error>::of(&QBluetoothServiceDiscoveryAgent::error),
            this, []() {
                qDebug() << "Service scan error!";
            });

    if (d->localDevice.isValid()) {
        qDebug() << d->localDevice.name();
        // Turn Bluetooth on
        d->localDevice.powerOn();

        // Make it visible to others
        //localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

        scannForDevices();
    }
    else {
        qDebug() << "no bluetooth device found";
    }

}

BtComModel::~BtComModel()
{
    delete d;
}

bool BtComModel::connected() const
{
    return d->connected;
}

int BtComModel::connectingIndex() const
{
    return d->connectingIndex;
}

bool BtComModel::bluetoothAvailable() const
{
    return d->localDevice.isValid();
}

void BtComModel::scannForDevices()
{
    if (scanning()) {
        qDebug() << "Already scanning...";
        return;
    }

    d->devices.clear();
    d->deviceAddrs.clear();

#ifdef Q_OS_ANDROID
    QMap<QString,QString> devMap;
    devMap = d->getKnownDevices();
    qDebug() << devMap;

    auto i = devMap.constBegin();
    while (i != devMap.constEnd()) {
        d->devices += i.value();
        d->deviceAddrs += i.key();
        ++i;
    }

#else

    d->discoveryAgent->setUuidFilter(QBluetoothUuid(QBluetoothUuid::SerialPort));
    //d->discoveryAgent->start(QBluetoothServiceDiscoveryAgent::FullDiscovery);
    d->discoveryAgent->start(QBluetoothServiceDiscoveryAgent::MinimalDiscovery);
#endif

    emit devicesChanged();
    emit scanningChanged();
}

const QStringList BtComModel::devices() const
{
    return d->devices;
}

const QStringList BtComModel::deviceAddrs() const
{
    return d->deviceAddrs;
}

bool BtComModel::scanning() const
{
    return d->discoveryAgent->isActive();
}

void BtComModel::connectToService(int index)
{
    qDebug() << "Try connect to " << index;
    if (index < 0 || index >= d->devices.size()) {
        qDebug() << "Bad device index";
        return;
    }
    d->connectingIndex = index;
    emit connectedChanged();

    if (d->socket) {
        delete d->socket;
    }
    d->socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    d->socket->connectToService(QBluetoothAddress(d->deviceAddrs[index]), QBluetoothUuid(QBluetoothUuid::SerialPort));

    connect(d->socket, &QBluetoothSocket::readyRead, this, [this]() { d->readSocket(); });
    connect(d->socket, &QBluetoothSocket::connected, this, [this]() {
        d->connectingIndex = -1;
        d->connected = true;
        emit connectedChanged();
        qDebug() << "Server Connected";
    });
    connect(d->socket, &QBluetoothSocket::disconnected, this, [this]() {
        d->connectingIndex = -1;
        d->connected = false;
        emit connectedChanged();
        qDebug() << "Server Disconnected";
    });
    connect(d->socket, QOverload<QBluetoothSocket::SocketError>::of(&QBluetoothSocket::error), this,
    [this](QBluetoothSocket::SocketError error) {
        d->connectingIndex = -1;
        emit connectedChanged();
        qDebug() << "Socket error:" << error;
    });

    connect(d->socket, &QBluetoothSocket::stateChanged, this, [](QBluetoothSocket::SocketState state) {
        qDebug() << "Socket stateChanged:" << state;
    });
}

bool BtComModel::sendString(const QString &str)
{
    if (!d->socket) {
        return false;
    }

    d->socket->write(str.toUtf8());
    return true;
}

