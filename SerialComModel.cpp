/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "SerialComModel.h"

#include <QSerialPort>

#include <QVector>
#include <QTimer>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

#include <QFileInfo>

class BtComModel::Private {
public:

    Private(BtComModel *q_): q(q_) {}

    void readSocket() {
        if (!serialport.isOpen()) {
            qDebug() << "No socket";
            return;
        }
        while (serialport.canReadLine()) {
            QByteArray btData = serialport.readLine();
            while (btData.endsWith('\r') || btData.endsWith('\n')) {
                btData.remove(btData.size()-1, 1);
            }
            emit q->dataReceived(btData);
        }
    }

    BtComModel *q;
    QStringList devices;
    QStringList deviceAddrs;
    int         connectingIndex=-1;
    QSerialPort serialport;
};

BtComModel::BtComModel(QObject *parent) : QObject(parent), d(new Private(this))
{
    connect(&d->serialport, &QSerialPort::readyRead, this, [this]() { d->readSocket(); });
    scannForDevices();
}

BtComModel::~BtComModel()
{
    delete d;
}

bool BtComModel::connected() const
{
    return d->serialport.isOpen();
}

int BtComModel::connectingIndex() const
{
    return d->connectingIndex;
}

bool BtComModel::bluetoothAvailable() const
{
    return true;
}

void BtComModel::scannForDevices()
{
    if (scanning()) {
        qDebug() << "Already scanning...";
        return;
    }

    d->devices = QStringList({ "/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2", "/dev/ttyACM0", "/dev/ttyACM1", "/dev/ttyACM2" });

    for (int i=d->devices.size()-1; i>=0; --i) {
        if (!QFileInfo::exists(d->devices[i])) {
            d->devices.removeAt(i);
        }
    }

    d->deviceAddrs = d->devices;

    emit devicesChanged();
    emit scanningChanged();
}

const QStringList BtComModel::devices() const
{
    return d->devices;
}

const QStringList BtComModel::deviceAddrs() const
{
    return d->deviceAddrs;
}

bool BtComModel::scanning() const
{
    return false;
}

void BtComModel::connectToService(int index)
{
    qDebug() << "Try connect to " << index;
    if (index < 0 || index >= d->devices.size()) {
        qDebug() << "Bad device index";
        return;
    }
    d->connectingIndex = index;
    emit connectedChanged();

    d->serialport.setPortName(d->devices[index]);
    d->serialport.setBaudRate(9600);
    d->serialport.setParity(QSerialPort::NoParity);
    d->serialport.setDataBits(QSerialPort::Data8);
    d->serialport.setFlowControl(QSerialPort::NoFlowControl);

    d->serialport.open(QIODevice::ReadWrite);


    emit connectedChanged();
}

bool BtComModel::sendString(const QString &str)
{
    if (!d->serialport.isOpen()) {
        return false;
    }

    d->serialport.write(str.toUtf8());
    return true;
}

